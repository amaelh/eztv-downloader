FROM python:3.9-bullseye

############################################################################
# System
############################################################################
# Install dependencies, setup user
# With this change to rsyslog, no need to pass the --privileged flag with Debian bullseye
RUN apt-get update && apt-get install -y \
        cron \
        rsyslog \
        --no-install-recommends \
    && rm -rf /var/lib/apt/lists/* \
    && groupadd -g 1000 eztv-downloader \
    && useradd -r -s /bin/sh -g 1000 -u 1000 eztv-downloader \
    && sed -i '/imklog/s/^/#/' /etc/rsyslog.conf

############################################################################
# Application
############################################################################
# Requirements on a separate layer, they evolve less frequently than our code
COPY ./requirements.txt /
RUN pip3 install -r /requirements.txt

# Specific code
COPY ./src/* /eztv-downloader/

RUN crontab -u eztv-downloader /eztv-downloader/eztv.crontab \
    && crontab -u eztv-downloader -l \
    && touch /var/log/cron.log && chmod a+w /var/log/cron.log

############################################################################
# Runtime
############################################################################
VOLUME ["/eztv-downloader/shows"]

WORKDIR /eztv-downloader/

# Define default command
CMD ["/eztv-downloader/entrypoint.sh"]
