eztv-downloader
===============

Automatically download new torrent files for your favorite TV Shows from EZTV. This image contains a python script called every 30 minutes to check for new episodes of your favorite TV shows on eztv.


## Add a new show
Imagine you want to watch The Big Bang Theory, and the last episode you have seen is Season 5, Episode 3 :
```
docker exec -it $(docker ps -a | grep registry.gitlab.com/amaelh/eztv-downloader | awk '{print $1}') python /eztv-downloader/eztv-addTVshow.py "the big bang theory" 5 3
```


## Show configurations
All show configurations are located in /eztv-downloader/shows/. You are strongly encouraged to mount this to a host folder to not loose your current status when downloading a new image. You can also directly use this folder to manually add a show, given you know the show ID on eztv.


## Misc. configurations
Configuration is done by setting environment variables :
```
  - EZTV_URL=https://eztv.re/
  - DOWNLOAD_FOLDER=/downloads/
  - LOG_LEVEL=INFO
```
