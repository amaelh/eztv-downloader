# Below are all project specific targets
test: test-os test-app

test-app: test-os
	@echo "##########################################################################"
	@echo "Testing single architecture image : Application"
	docker run --env EZTV_URL=https://eztv.re --env LOG_LEVEL='DEBUG' --rm $(DOCKER_IMAGE_TAGNAME)-$(BUILD_ARCH)  sh -c 'python /eztv-downloader/eztv-addTVshow.py "the big bang theory" 5 3 && echo && /bin/echo "=> Add new show script OK" && echo ""'
