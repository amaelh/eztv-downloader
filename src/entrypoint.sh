#!/bin/bash

# Output docker-compose variables to env file
printenv >> /etc/environment

# Run cron
rsyslogd && cron
tail -f /var/log/syslog /var/log/cron.log
