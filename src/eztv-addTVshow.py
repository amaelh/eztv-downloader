#!/usr/bin/env python
# -*- coding: utf-8 -*-

import configparser, sys, urllib.request, urllib.error, re, traceback, os, eztvLogger
from werkzeug.utils import secure_filename

#print("Env thinks the user is [%s]" % (os.getlogin()))
#print("Effective user is [%s]" % (getpass.getuser()))

EXIT_SYNTAX_ERROR = 2
EXIT_OTHER_ERROR = 1

if (len(sys.argv) < 4):
    print("usage: " + sys.argv[0] + " \"TV Show\" Season Episode [quality]")
    sys.exit(EXIT_SYNTAX_ERROR)

# Read site URL from environment variables
siteURL = os.getenv('EZTV_URL')
if (not siteURL):
    print("=> ERROR : You have to export EZTV_URL environment variable before running this script. Ex : export EZTV_URL='https://eztv.re'")
    sys.exit(EXIT_SYNTAX_ERROR)

show = re.sub(' ', '.*', re.sub('^the ', '', sys.argv[1].title().lower()))
season = sys.argv[2]
episode = sys.argv[3]

showListURL = siteURL+"/showlist/"

eztvLogger.logging.debug("Search " + showListURL + " for show=<" + show + ">, season=<" +
                         season + ">, episode=<" + episode + ">")

if len(sys.argv) == 5:
    quality = sys.argv[4]
else:
    quality = 'either'

if re.match('^(1080p|720p|hdtv|either)$', quality, re.I):
    quality.lower()
else:
    print("quality must be either HDTV, 720p or 1080p")
    sys.exit(EXIT_SYNTAX_ERROR)

try:
    d = dict()
    f = urllib.parse.urlencode(d).encode('utf-8')
    request_headers = {'User-agent': 'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11'}
    req = urllib.request.Request(showListURL, f, request_headers)

    url = urllib.request.urlopen(req)
    site = url.read().decode('utf-8', errors='ignore')
    url.close()

    #eztvLogger.logging.debug("Site response=" + site)

    reg = "/shows/\d+/.*" + show
    match = str(re.findall(reg, site, re.I))
    eztvLogger.logging.debug("reg = <" + reg + ">, match = " + match)

    liste = re.findall(r"(\/shows\/)(\d+)(/[^/]*\/)(.*)", match)
    eztvLogger.logging.debug("liste = " + str(liste))

    id = liste[0][1]
    filename = liste[0][2]

    config = configparser.RawConfigParser()

    config.add_section('Serie')
    config.set('Serie', 'id', id)
    config.set('Serie', 'Season', season)
    config.set('Serie', 'Episode', episode)
    config.set('Serie', 'Quality', quality)

    name = '/eztv-downloader/shows/' + secure_filename(filename.replace('/', '')) + '.cfg'
    with open(name, 'w') as configfile:
        config.write(configfile)
        # Change owner to eztv-downloader
        os.chown(name, 1000, 1000)
        eztvLogger.logging.info("TV Show " + sys.argv[1].title() + " added!")

except:
    print("=> ERROR : Couldn't find a TV Show with this name!")
    traceback.print_exc(file=sys.stdout)
    sys.exit(EXIT_OTHER_ERROR)
