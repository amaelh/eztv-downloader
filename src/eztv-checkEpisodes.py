#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib, urllib.request, urllib.error, re, configparser, sys, os, traceback, eztvLogger, lxml.html

# Read configuration from environment variables
siteURL = os.getenv('EZTV_URL')
downloadFolder = os.getenv('DOWNLOAD_FOLDER')

def download(url):
    file_name = url.split('/')[-1]
    request_headers = {'User-agent': 'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11'}
    req = urllib.request.Request(url, None, request_headers)
    u = urllib.request.urlopen(req)

    f = open(downloadFolder + "/" + file_name, 'wb')
    meta = u.info()
    file_size = int(meta.get("Content-Length")[0])
    eztvLogger.logging.info("Downloading: %s Bytes: %s" % (file_name, file_size))

    file_size_dl = 0
    block_sz = 8192
    while True:
        buffer = u.read(block_sz)
        if not buffer:
            break

        file_size_dl += len(buffer)
        f.write(buffer)
        status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
        status = status + chr(8)*(len(status)+1)
        #print(status,
#        eztvLogger.logging.info(status))

    f.close()


def getEpisode():
    try:
        trace = "Checking website " + siteURL
        eztvLogger.logging.info(trace)

        for cfg in os.listdir("/eztv-downloader/shows/"):
            if cfg.endswith(".cfg"):
                trace = "Checking for new episodes regarding : " + cfg
                config = configparser.RawConfigParser()
                config.read("/eztv-downloader/shows/" + cfg)

                if config.has_option('Serie', 'Quality'):
                    serie_quality = config.get('Serie', 'Quality')
                else:
                    serie_quality = 'undefined'

                episodio_local=[config.getint('Serie', 'Season'), config.getint('Serie', 'Episode')]
                episodios_novos = []

                url = siteURL + "shows/" + config.get('Serie', 'id') + "/"

                request_headers = {'User-agent': 'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11'}
                trace = "Accessing " + url + " - searching for " + cfg.replace(".cfg", "") + " > S" + config.get('Serie', 'Season') + "E" + config.get('Serie', 'Episode')
                eztvLogger.logging.debug(trace)
                print(trace)

                d = dict()
                f = urllib.parse.urlencode(d).encode('utf-8')

                req = urllib.request.Request(url, f, request_headers)
                serie = urllib.request.urlopen(req)
                episodios = serie.read()
                serie.close()

                doc = lxml.html.fromstring(episodios)
                links = doc.xpath('//a/@href')

                torrents = []
                for link in links:
                    if link.endswith(".torrent"):
                        torrents.append(re.findall(r'(.*(?:s|season|\.|_)(\d{1,2})(?:e|x|episode)(\d{1,2}).*)', link, re.I))

                episodios_processados = []

                for torrent in torrents:
                    if (len(torrent)==0):
                        pass
                    else:
                        if serie_quality == '1080p' and not re.search('1080p', torrent[0][0], re.I):
                            continue
                        if serie_quality == '720p' and not re.search('720p', torrent[0][0], re.I):
                            continue
                        if serie_quality == 'hdtv' and ( re.search('720p', torrent[0][0], re.I) or re.search('1080p', torrent[0][0], re.I) ):
                            continue

                        episodio = [int(torrent[0][1]), int(torrent[0][2])]
                        if episodio_local >= episodio or episodios_processados.__contains__(episodio):
                            pass
                        else:
                            episodios_novos.append(urllib.parse.quote(torrent[0][0]).replace("https%3A", "https:"))
                            episodios_processados.append(episodio)

                if len(episodios_novos) > 0:
                    eztvLogger.logging.info(str(len(episodios_novos)) + " new episodes from " + cfg.replace(".cfg", ""))
                    for episodio_novo in episodios_novos:
#                        print("Downloading : " + str(episodio_novo))
                        download(str(episodio_novo))
                    ultimo = re.split('E|X', re.search('(\d+[eExX]\d+)', episodios_novos[0]).group(0).upper())
                    config.set('Serie', 'Season', int(ultimo[0]))
                    config.set('Serie', 'Episode', int(ultimo[1]))
                    with open("/eztv-downloader/shows/" + cfg, 'w') as configfile:
                        config.write(configfile)

                else:
                    eztvLogger.logging.debug("No new episodes from " + cfg.replace(".cfg", ""))

    except:
        print("Error!")
        traceback.print_exc(file=sys.stdout)

getEpisode()
