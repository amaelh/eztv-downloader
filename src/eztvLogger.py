#!/usr/bin/env python

import logging, sys, os

EXIT_SYNTAX_ERROR = 2

# Read configuration from environment variables
logLevel = os.getenv('LOG_LEVEL')
# Default logginng level
if (not logLevel):
    logLevel='INFO'

# Log to stdout for docker containers
logging.basicConfig(level=logLevel,
        format='%(asctime)s %(levelname)s %(message)s',
        stream=sys.stdout)

